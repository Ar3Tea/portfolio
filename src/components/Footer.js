import React, { Component } from 'react';
//import logoReact from '../styles/logo.svg';

class Footer extends Component {
  render() {
    return (
      <div>
        <hr />
        Made  with  <i className="fa fa-heart" />  and  <i className="fab fa-react" />,  by <a href="https://github.com/Ar3Tea" target="_blank" rel="noopener noreferrer">me</a>!
      </div>
    );
  }
}

export default Footer;
